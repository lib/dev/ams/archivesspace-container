from __future__ import annotations

import hashlib
import os
import re
import subprocess
from dataclasses import dataclass
from pathlib import Path
from tempfile import NamedTemporaryFile, _TemporaryFileWrapper
from typing import Callable, NewType, Sequence, TypeVar
import shlex

from typing_extensions import ParamSpec

from aspace_plugins.constants import cache_dir
from aspace_plugins.models import GitPluginSource, LockedGitPluginSource
from aspace_plugins.threading import KeyedLock

P = ParamSpec("P")
T = TypeVar("T")

AbsRepoUrl = NewType("AbsRepoUrl", str)


class GitError(RuntimeError):
    pass


@dataclass(frozen=True)
class FetchedRepoRef:
    repo_dir: Path
    remote_ref: str
    resolved_ref: str


@dataclass
class GitContext:
    extra_ssh_known_hosts_lines: Sequence[str] | None = None
    _tmp_known_hosts_file: _TemporaryFileWrapper[str] | None = None

    def get_extra_ssh_known_hosts_file(self) -> Path:
        if self._tmp_known_hosts_file is None:
            host_keys_file_content = "\n".join(self.extra_ssh_known_hosts_lines or [])
            self._tmp_known_hosts_file = NamedTemporaryFile("w+")
            print(host_keys_file_content, flush=True, file=self._tmp_known_hosts_file)
        return Path(self._tmp_known_hosts_file.name)

    def get_env(self) -> dict[str, str]:
        env = {}
        if self.extra_ssh_known_hosts_lines:
            overridden_known_hosts = shlex.quote(
                f"UserKnownHostsFile=~/.ssh/known_hosts "
                f"{self.get_extra_ssh_known_hosts_file()}"
            )
            env["GIT_SSH_COMMAND"] = f"ssh -o {overridden_known_hosts}"
        return env

    def run(
        self,
        cmd: Sequence[str | os.PathLike[str]],
        check: bool = True,
        extra_env: dict[str, str] | None = None,
    ) -> subprocess.CompletedProcess[str]:
        env = {**os.environ, **self.get_env(), **(extra_env or {})}
        try:
            return subprocess.run(
                cmd,
                check=check,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                encoding="utf-8",
                env=env,
            )
        except subprocess.CalledProcessError as e:
            raise GitError(
                f"Failed to execute git command; command: {e.cmd}; command output:\n"
                f"{e.stdout}"
            ) from e


def resolve_relative_repo_url(repo_url: str, *, base_dir: Path) -> AbsRepoUrl:
    if not base_dir.is_absolute():
        raise ValueError(f"base_dir must be absolute: {base_dir}")
    if repo_url.startswith("./"):
        return AbsRepoUrl(str(base_dir / repo_url))
    return AbsRepoUrl(repo_url)


def parse_abs_repo_url(repo_url: str) -> AbsRepoUrl:
    if repo_url.startswith("./"):
        raise ValueError(
            f"repo_url starts with './' must not be a relative file path: {repo_url=}"
        )
    return AbsRepoUrl(repo_url)


def get_repo_cache_path(repo_url: AbsRepoUrl) -> Path:
    name = "repo-" + hashlib.sha256(repo_url.encode()).hexdigest()[:16] + ".git"
    return cache_dir / name


def is_sha1_digest(ref: str) -> bool:
    return bool(re.match(r"^[0-9a-f]{40}$", ref))


def repo_writer(func: Callable[P, T]) -> Callable[P, T]:
    repolocks = KeyedLock[Path]()

    def repo_writer(*args: P.args, **kwargs: P.kwargs) -> T:
        repo_url = kwargs.get("repo_url")
        if not isinstance(repo_url, str):
            raise TypeError("repo_writer decorator did not receive a repo_url kwarg")
        repo_url = parse_abs_repo_url(repo_url)

        repo_path = get_repo_cache_path(repo_url)
        with repolocks.acquire(repo_path):
            return func(*args, **kwargs)

    return repo_writer


@repo_writer
def fetch_plugin_repo_ref(
    *, repo_url: AbsRepoUrl, ref: str, ctx: GitContext
) -> FetchedRepoRef:
    repo_dir = get_repo_cache_path(repo_url)
    repo_dir.parent.mkdir(parents=True, exist_ok=True)
    if not repo_dir.exists():
        ctx.run(["git", "init", "--bare", repo_dir])
        ctx.run(["git", "-C", repo_dir, "remote", "add", "origin", repo_url])
    # Skip fetching if the ref is commit hash which exists
    ref_exists = is_sha1_digest(ref) and ctx.run(
        ["git", "-C", repo_dir, "rev-parse", "--verify", f"{ref}^{{commit}}"],
        check=False,
    )
    if ref_exists and ref_exists.returncode == 0:
        return FetchedRepoRef(repo_dir=repo_dir, remote_ref=ref, resolved_ref=ref)

    ctx.run(["git", "-C", repo_dir, "fetch", "origin", "--", ref])
    # Ensure the ref resolves to a commit hash
    resolved_ref = ctx.run(
        ["git", "-C", repo_dir, "rev-parse", "--verify", "FETCH_HEAD^{commit}"]
    ).stdout.strip()
    if not re.match("^[0-9a-f]{40}$", resolved_ref):
        raise ValueError(
            f"unexpected git rev-parse output for FETCH_HEAD: {resolved_ref}"
        )
    return FetchedRepoRef(repo_dir=repo_dir, remote_ref=ref, resolved_ref=resolved_ref)


def resolve_git_source(
    git_source: GitPluginSource, base_dir: Path
) -> LockedGitPluginSource:
    ctx = GitContext(extra_ssh_known_hosts_lines=git_source.extra_ssh_known_hosts_lines)
    fetched_ref = fetch_plugin_repo_ref(
        repo_url=resolve_relative_repo_url(git_source.repo, base_dir=base_dir),
        ref=git_source.ref,
        ctx=ctx,
    )
    return LockedGitPluginSource(
        repo=git_source.repo, ref=git_source.ref, resolved_ref=fetched_ref.resolved_ref
    )


def copy_repo_files(
    *,
    repo_url: str,
    ref: str,
    dest_dir: Path,
    git_source: GitPluginSource,
    base_dir: Path,
) -> None:
    ctx = GitContext(extra_ssh_known_hosts_lines=git_source.extra_ssh_known_hosts_lines)
    fetched_ref = fetch_plugin_repo_ref(
        repo_url=resolve_relative_repo_url(repo_url, base_dir=base_dir),
        ref=ref,
        ctx=ctx,
    )
    dest_dir.mkdir(exist_ok=False)

    subprocess.run(
        'git -C "${REPO_DIR:?}" archive -- "${REVISION:?}" | tar -x -C "${DEST_DIR:?}"',
        shell=True,
        env={
            **os.environ,
            "REPO_DIR": str(fetched_ref.repo_dir),
            "REVISION": fetched_ref.resolved_ref,
            "DEST_DIR": str(dest_dir),
        },
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        encoding="utf-8",
    )
