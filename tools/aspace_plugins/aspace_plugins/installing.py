import hashlib
import io
import json
import os
import shutil
import subprocess
import sys
from dataclasses import dataclass, field
from pathlib import Path
from typing import Sequence

from aspace_plugins.constants import (
    aspace_install_dir,
    cache_dir,
    default_gid,
    default_uid,
    runtime_uid,
)
from aspace_plugins.git import copy_repo_files
from aspace_plugins.models import (
    ConfigOnlyPluginSource,
    GitPluginSource,
    GlobalFileOverride,
    LockedConfigOnlyPluginSource,
    LockedGitPluginSource,
    PluginConfig,
    RootLockedPluginConfig,
    RootPluginConfig,
    load_plugin_config,
)


@dataclass
class FileInstallResult:
    from_cache: bool


@dataclass
class GemfileInstallResult:
    plugin_dir: Path
    installed_gemfile: Path | None


@dataclass
class InstallResult:
    config: PluginConfig
    plugin_dir: Path
    from_cache: bool = False
    container_config_files: set[Path] = field(default_factory=set)

    def print_report(self) -> None:
        msg = [f"[{self.config.ordering_priority}] {self.plugin_dir.name}: installed"]
        if self.from_cache:
            msg.append(" (from cache)")
        for file in sorted(self.container_config_files):
            msg.append(f"; with {file}")
        print("".join(msg), file=sys.stderr)


def _run_and_log(
    cmd: Sequence[str | os.PathLike[str]],
    log: io.StringIO,
    cwd: Path | None = None,
) -> subprocess.CompletedProcess[str]:
    proc = subprocess.run(
        cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding="utf-8", cwd=cwd
    )
    print(f"run(cmd={cmd!r}, ...) -> exit status: {proc.returncode}", file=log)
    log.write(proc.stdout)
    proc.check_returncode()
    return proc


def initialised_files_cache_dir(
    *, plugin_name: str, config: PluginConfig, lock: LockedGitPluginSource
) -> Path:
    version_key = hashlib.sha256(lock.resolved_ref.encode()).hexdigest()
    return cache_dir / "initialized-plugins" / plugin_name / version_key


def install_plugin_files(
    *,
    plugin_conf_dir: Path,
    config: PluginConfig,
    lock: LockedGitPluginSource,
    dest_dir: Path,
) -> FileInstallResult:
    # TODO: maybe stop caching here. We used to install gems per-plugin, which
    #   was slow, but now all the caching saves is exporting files from the
    #   git repo, which should be quite fast anyway, as we cache the git repos
    #   with revisions fetched.
    cached_files_dir = initialised_files_cache_dir(
        plugin_name=plugin_conf_dir.name, config=config, lock=lock
    )
    if cached_files_dir.exists():
        shutil.copytree(cached_files_dir, dest_dir)
        return FileInstallResult(from_cache=True)

    assert isinstance(config.source, GitPluginSource)
    copy_repo_files(
        repo_url=lock.repo,
        ref=lock.resolved_ref,
        dest_dir=dest_dir,
        git_source=config.source,
        base_dir=plugin_conf_dir,
    )

    # cache the files for reuse next time
    shutil.copytree(dest_dir, cached_files_dir)

    return FileInstallResult(from_cache=False)


def install_container_config_file(
    *,
    src: str | bytes | Path,
    dest_name: str | Path,
    dest_dir: Path,
) -> None:
    dest_name = Path(dest_name)
    if dest_name.is_absolute() or len(dest_name.parts) != 1:
        raise ValueError(f"dest_name must be a filename with no dirs: {dest_name}")
    container_dir = dest_dir / "container-config"
    container_dir.mkdir(exist_ok=True, parents=True)
    dest = container_dir / dest_name

    if isinstance(src, Path):
        shutil.copy2(src, container_dir)
    else:
        dest.write_bytes(src.encode() if isinstance(src, str) else src)

    if dest_name.match("*.sh"):
        dest.chmod(0o755)


def install_plugin_ordering_priority(
    *,
    ordering_priority: int,
    dest_dir: Path,
) -> None:
    install_container_config_file(
        src=f"{ordering_priority}",
        dest_name="ordering-priority",
        dest_dir=dest_dir,
    )


def install_global_file_overrides(
    *, global_file_overrides: Sequence[GlobalFileOverride], plugin_dir: Path
) -> None:
    for override in global_file_overrides:
        assert not override.src.is_absolute()
        assert not override.dst.is_absolute()

        src = plugin_dir / override.src
        dst = aspace_install_dir / override.dst
        dst.parent.mkdir(exist_ok=True)
        shutil.copy2(src, dst)


def configure_file_permissions(
    *, writable_files: Sequence[Path], plugin_install_dir: Path
) -> None:
    # By default, all plugin files are owned by the root user and not writable
    # by the archivesspace user at runtime.
    for dirpath, dirnames, filenames in os.walk(plugin_install_dir):
        os.chown(dirpath, default_uid, default_gid)
        for filename in filenames:
            os.chown(os.path.join(dirpath, filename), default_uid, default_gid)

    # Writable files are owned by the archivesspace user and writable by it.
    for path in writable_files:
        abs_path: Path
        # Paths starting with / are relative to the ArchivesSpace install dir.
        # Otherwise they're relative to the plugin dir they're configuring.
        if path.is_absolute():
            abs_path = aspace_install_dir / path.relative_to("/")
        else:
            abs_path = plugin_install_dir / path

        if not abs_path.parent.exists():
            abs_path.parent.mkdir(parents=True)
        if not abs_path.exists():
            abs_path.touch()
        os.chown(abs_path, runtime_uid, default_gid)
        mode = abs_path.stat().st_mode
        new_mode = mode | 0o200  # make mode writable for user
        if new_mode != mode:
            abs_path.chmod(new_mode)


def validate_config_lock(
    config: RootPluginConfig, lock: RootLockedPluginConfig | None
) -> RootLockedPluginConfig:
    if lock is None:
        raise ValueError(
            "Cannot install plugin: Plugin's config source is not locked to an "
            "exact revision"
        )
    if not lock.is_up_to_date_with_config(config):
        raise ValueError(
            "Cannot install plugin: Plugin's lock file is not up to date with "
            "the config file"
        )
    return lock


def install_plugin_source(plugin_dir: Path) -> InstallResult:
    config_file, config, lock_file, lock = load_plugin_config(plugin_dir)
    lock = validate_config_lock(config, lock)
    result = InstallResult(config=config.plugin, plugin_dir=plugin_dir)

    plugin_install_dir = aspace_install_dir / "plugins" / plugin_dir.name
    plugin_install_dir.parent.mkdir(exist_ok=True)
    if isinstance(lock.plugin.locked_source, LockedGitPluginSource):
        assert isinstance(config.plugin.source, GitPluginSource)
        install_result = install_plugin_files(
            plugin_conf_dir=plugin_dir,
            config=config.plugin,
            lock=lock.plugin.locked_source,
            dest_dir=plugin_install_dir,
        )
        result.from_cache = install_result.from_cache
    else:
        assert isinstance(lock.plugin.locked_source, LockedConfigOnlyPluginSource)
        assert isinstance(config.plugin.source, ConfigOnlyPluginSource)
        # nothing to do

    for file in ["config.rb", "docker-entrypoint.sh", "startup.rb"]:
        src = plugin_dir / file
        if not src.exists():
            continue
        install_container_config_file(
            src=src, dest_name=file, dest_dir=plugin_install_dir
        )
        result.container_config_files.add(Path(file))

    if config.plugin.global_file_overrides:
        install_global_file_overrides(
            global_file_overrides=config.plugin.global_file_overrides,
            plugin_dir=plugin_install_dir,
        )

    install_plugin_ordering_priority(
        ordering_priority=config.plugin.ordering_priority, dest_dir=plugin_install_dir
    )

    if config.plugin.writable_files:
        configure_file_permissions(
            writable_files=config.plugin.writable_files,
            plugin_install_dir=plugin_install_dir,
        )

    return result


def install_plugin_gemfile(plugin_dir: Path) -> GemfileInstallResult:
    plugin_src_gemfile = plugin_dir / "Gemfile"
    if not plugin_src_gemfile.exists():
        return GemfileInstallResult(plugin_dir=plugin_dir, installed_gemfile=None)

    plugin_install_dir = aspace_install_dir / "plugins" / plugin_dir.name
    plugin_install_dir.parent.mkdir(exist_ok=True)
    plugin_install_dir.mkdir(exist_ok=True)

    installed_gemfile = shutil.copy2(plugin_src_gemfile, plugin_install_dir)
    return GemfileInstallResult(
        plugin_dir=plugin_dir, installed_gemfile=installed_gemfile
    )


def create_plugins_build_config(plugin_conf_dirs: Sequence[Path]) -> Path:
    """Create an ArchivesSpace config file to build with plugins active.

    ArchivesSpace supports installing ruby gems for plugins at build time.
    Building with plugins configured results in gems for ArchivesSpace and
    plugins being resolved together, which prevents gem version conflicts that
    can arise when plugins are installed on top of an existing ArchivesSpace
    build.

    This function creates plugin dirs for each configured plugin containing just
    the Gemfile. It also creates an ArchivesSpace config file that enables these
    plugins. If ArchivesSpace is subsequently built with the config file
    enabled, the plugins' Gemfiles will be included overall.

    @return The path of the created config file
    """
    # TODO: we could selectively enable the plugin only in specific components,
    # rather than all.

    results = [install_plugin_gemfile(d) for d in plugin_conf_dirs]

    required_plugins = [r.plugin_dir.name for r in results if r.installed_gemfile]

    # JSON encoding of an array of strings should be valid ruby code
    config_content = f"AppConfig[:plugins] = {json.dumps(required_plugins)}\n"

    conf_file_path = aspace_install_dir / "build_plugin_conf.rb"
    with open(conf_file_path, "w") as conf_file:
        conf_file.write(config_content)

    return conf_file_path
