import contextlib
from threading import Lock
from typing import Generator, Generic, Hashable, NewType, TypeVar

_KT = TypeVar("_KT", bound=Hashable)
_Waiter = NewType("_Waiter", object)


class KeyedLock(Generic[_KT]):
    __metalock: Lock
    __locks: dict[_KT, tuple[Lock, set[_Waiter]]]

    def __init__(self) -> None:
        self.__metalock = Lock()
        self.__locks = {}

    @contextlib.contextmanager
    def acquire(self, key: _KT) -> Generator[_KT, None, None]:
        this = _Waiter(object())
        with self.__metalock:
            if key not in self.__locks:
                self.__locks[key] = (Lock(), set())
            lock, waiters = self.__locks[key]
            waiters.add(this)
        try:
            with lock:
                yield key
        finally:
            with self.__metalock:
                waiters.remove(this)
                if len(waiters) == 0:
                    del self.__locks[key]
