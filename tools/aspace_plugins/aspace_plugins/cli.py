from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from typing import Sequence
import traceback
from aspace_plugins import constants

import click

from aspace_plugins import installing, locking


@click.group()
def aspace_plugins() -> None:
    pass


def _default_plugin_conf_dirs() -> Sequence[Path]:
    plugins_dir = constants.default_plugin_dir
    if not constants.default_plugin_dir.exists():
        raise click.UsageError(
            f"No plugin dirs specified and the default {plugins_dir} does not exist."
        )
    return sorted(d for d in plugins_dir.iterdir() if d.is_dir())


@aspace_plugins.command("lock")
@click.argument(
    "plugin_conf_dir",
    nargs=-1,
    type=click.Path(exists=True, file_okay=False, path_type=Path),
)
def lock(plugin_conf_dir: Sequence[Path]) -> None:
    """
    Lock plugin sources by resolving their exact versions.
    """
    if not plugin_conf_dir:
        plugin_conf_dir = _default_plugin_conf_dirs()

    def _run(plugin_dir: Path) -> locking.LockResult:
        try:
            return locking.lock_plugin_source(plugin_dir.resolve())
        except Exception as e:
            err = "".join(traceback.format_exception(e))
            raise click.ClickException(
                f"Failed to lock plugin {plugin_dir}:\n{err}"
            ) from e

    with ThreadPoolExecutor() as executor:
        for result in executor.map(_run, plugin_conf_dir):
            result.print_report()


@aspace_plugins.command("install")
@click.argument(
    "plugin_conf_dir",
    nargs=-1,
    type=click.Path(exists=True, file_okay=False, path_type=Path),
)
def install(plugin_conf_dir: Sequence[Path]) -> None:
    """
    Install ArchivesSpace plugins according to plugin configs.
    """
    if not plugin_conf_dir:
        plugin_conf_dir = _default_plugin_conf_dirs()

    def _run(plugin_dir: Path) -> installing.InstallResult:
        try:
            return installing.install_plugin_source(plugin_dir.resolve())
        except Exception as e:
            err = "".join(traceback.format_exception(e))
            raise click.ClickException(
                f"Failed to install plugin {plugin_dir}:\n{err}"
            ) from e

    with ThreadPoolExecutor() as executor:
        for result in executor.map(_run, plugin_conf_dir):
            result.print_report()


@aspace_plugins.command("install-plugin-build-files")
@click.argument(
    "plugin_conf_dir",
    nargs=-1,
    type=click.Path(exists=True, file_okay=False, path_type=Path),
)
def create_build_config(plugin_conf_dir: Sequence[Path]) -> None:
    """Create an ArchivesSpace config file to build with plugins active.

    ArchivesSpace supports installing ruby gems for plugins at build time.
    Building with plugins configured results in gems for ArchivesSpace and
    plugins being resolved together, which prevents gem version conflicts that
    can arise when plugins are installed on top of an existing ArchivesSpace
    build.
    """

    for d in plugin_conf_dir:
        if constants.aspace_install_dir in d.parents:
            raise click.ClickException(
                "A plugin conf dir is inside the install dir. "
                f"ASPACE_LAUNCHER_BASE: {constants.aspace_install_dir}, dir: {d}"
            )

    installing.create_plugins_build_config(plugin_conf_dir)
