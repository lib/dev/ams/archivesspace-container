import os
from pathlib import Path
from typing import Final
import warnings

import platformdirs


def _get_envar_non_negative_int(envar_name: str, *, default: int) -> int:
    raw = os.environ.get(envar_name)
    try:
        if raw:
            id_ = int(raw)
            if id_ >= 0:
                return id_
            raise ValueError(f"id out of range: {id_}")
    except ValueError as e:
        warnings.warn(
            f"Environment variable {envar_name}={raw!r} is not a "
            f"non-negative integer: {e}"
        )
    return default


aspace_install_dir: Final[Path] = Path(
    os.environ.get("ASPACE_LAUNCHER_BASE") or "/opt/archivesspace"
)
cache_dir: Final[Path] = Path(
    os.environ.get("ASPACE_PLUGINS_CACHE_DIR")
    or platformdirs.user_cache_dir(appname="aspace_plugins")
)
default_plugin_dir: Final[Path] = Path(
    os.environ.get("ASPACE_PLUGINS_DEFAULT_CONF_DIR") or "archivesspace-plugins"
)
tmp_dir: Final[Path] = Path(
    os.environ.get("ASPACE_PLUGINS_TMP_DIR")
    or platformdirs.user_runtime_path(appname="arcspace_plugins")
)
default_uid: Final[int] = _get_envar_non_negative_int(
    "ASPACE_PLUGINS_DEFAULT_UID", default=os.geteuid()
)
default_gid: Final[int] = _get_envar_non_negative_int(
    "ASPACE_PLUGINS_DEFAULT_GID", default=os.getegid()
)
runtime_uid: Final[int] = _get_envar_non_negative_int(
    "ASPACE_PLUGINS_RUNTIME_UID", default=1000
)
