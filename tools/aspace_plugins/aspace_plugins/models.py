from __future__ import annotations
from enum import StrEnum

import os
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Annotated, Literal, Sequence, TypeAlias, TypeVar

import tomli
from pydantic import AfterValidator, BaseModel, ConfigDict, Field, StringConstraints

_TModel = TypeVar("_TModel", bound=BaseModel)


def parse_toml_file(*, model: type[_TModel], file: Path) -> _TModel:
    toml_data = tomli.loads(file.read_text())
    return model.model_validate(toml_data)


def parse_json_file(*, model: type[_TModel], file: Path) -> _TModel:
    return model.model_validate_json(file.read_text())


def write_json_file(*, value: BaseModel, file: Path) -> None:
    # Dump to JSON to serialise python types
    json_value = value.model_dump_json(indent=2)
    with NamedTemporaryFile(mode="w", dir=file.parent, delete=False) as new_file:
        new_file.write(json_value)
    os.replace(src=new_file.name, dst=file)


def load_plugin_config(
    plugin_dir: Path,
) -> tuple[Path, RootPluginConfig, Path, RootLockedPluginConfig | None]:
    config_file = plugin_dir / "plugin.toml"
    lock_file = plugin_dir / "plugin.lock"

    try:
        config = parse_toml_file(file=config_file, model=RootPluginConfig)
    except Exception as e:
        raise ValueError(f"Failed to load {config_file}: {e}") from e
    try:
        if lock_file.exists():
            lock = parse_json_file(file=lock_file, model=RootLockedPluginConfig)
        else:
            lock = None
    except Exception as e:
        raise ValueError(f"Failed to load {lock_file}: {e}") from e

    return config_file, config, lock_file, lock


class StrictBaseModel(BaseModel):
    model_config = ConfigDict(extra="forbid")


AnyPluginSource: TypeAlias = "GitPluginSource | ConfigOnlyPluginSource"


class RootPluginConfig(StrictBaseModel):
    plugin: PluginConfig


def require_relative_path(path: Path) -> Path:
    if path.is_absolute():
        raise ValueError("path must be relative")
    return path


RelativePath = Annotated[Path, AfterValidator(require_relative_path)]


class GlobalFileOverride(BaseModel):
    src: RelativePath
    dst: RelativePath


class PluginConfig(StrictBaseModel):
    source: AnyPluginSource = Field(discriminator="type")
    writable_files: Sequence[Path] | None = None
    global_file_overrides: Sequence[GlobalFileOverride] | None = None
    """Files to copy from the installed plugin dir to the ArchivesSpace dir.

    Used when a plugin needs to override ArchivesSpace files, because it's not
    possible to affect a required change within the normal bounds of a plugin.
    """
    ordering_priority: int = 50


class SourceType(StrEnum):
    GIT = "git"
    CONFIG_ONLY = "config-only"


class ConfigOnlyPluginSource(StrictBaseModel):
    """
    A plugin source for plugins that only add container config, no plugin files.
    """

    type: Literal["config-only"] = "config-only"


class GitPluginSource(StrictBaseModel):
    """
    A install a plugin by fetching its files from a Git repo.
    """

    type: Literal["git"] = "git"
    repo: str
    "The git repo to install the plugin from."
    ref: str
    "A ref (branch, tag, ...) for the commit to install the plugin from."
    extra_ssh_known_hosts_lines: Sequence[str] | None = None
    """
    Additional public key fingerprints for git SSH operations.

    For git to securely connect to the repo host via SSH, it needs to know one
    or more public key fingerprints of the repo host. Entries in this list are
    temporarily included in a known_hosts file when running git operations for
    this source.
    """


AnyLockedPluginSource: TypeAlias = (
    "LockedGitPluginSource | LockedConfigOnlyPluginSource"
)


class RootLockedPluginConfig(StrictBaseModel):
    plugin: LockedPluginConfig

    @classmethod
    def from_locked_source(
        cls, locked_source: AnyLockedPluginSource
    ) -> RootLockedPluginConfig:
        return RootLockedPluginConfig(
            plugin=LockedPluginConfig(locked_source=locked_source)
        )

    def is_up_to_date_with_config(self, config: RootPluginConfig) -> bool:
        return self.plugin.locked_source.is_up_to_date_with_source(config.plugin.source)


class LockedPluginConfig(StrictBaseModel):
    locked_source: AnyLockedPluginSource


class LockedConfigOnlyPluginSource(StrictBaseModel):
    type: Literal["config-only"] = "config-only"

    def is_up_to_date_with_source(self, source: AnyPluginSource) -> bool:
        return isinstance(source, ConfigOnlyPluginSource)


class LockedGitPluginSource(StrictBaseModel):
    type: Literal["git"] = "git"
    repo: str
    ref: str
    resolved_ref: Annotated[str, StringConstraints(pattern=r"^[0-9a-f]{40}$")]
    "The `ref` resolved to a git commit hash."

    def is_up_to_date_with_source(self, source: AnyPluginSource) -> bool:
        return (
            isinstance(source, GitPluginSource)
            and self.type == source.type
            and self.repo == source.repo
            and self.ref == source.ref
        )
