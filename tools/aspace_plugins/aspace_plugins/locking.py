import sys
from dataclasses import dataclass
from pathlib import Path
from typing import Literal

from aspace_plugins.git import resolve_git_source
from aspace_plugins.models import (
    AnyLockedPluginSource,
    ConfigOnlyPluginSource,
    GitPluginSource,
    LockedConfigOnlyPluginSource,
    LockedGitPluginSource,
    RootLockedPluginConfig,
    load_plugin_config,
    write_json_file,
)


@dataclass
class LockResult:
    plugin_dir: Path
    locked_source: AnyLockedPluginSource
    action: Literal["resolved", "up-to-date"]

    def print_report(self) -> None:
        if isinstance(self.locked_source, LockedGitPluginSource):
            detail = (
                f"{self.locked_source.repo}#{self.locked_source.ref}"
                f" -> {self.locked_source.resolved_ref}"
            )
        else:
            assert isinstance(self.locked_source, LockedConfigOnlyPluginSource)
            detail = "(config-only)"
        print(f"{self.plugin_dir.name}: {self.action}: {detail}", file=sys.stderr)


def lock_plugin_source(plugin_dir: Path) -> LockResult:
    config_file, config, lock_file, lock = load_plugin_config(plugin_dir)

    locked_source: AnyLockedPluginSource
    if isinstance(config.plugin.source, GitPluginSource):
        locked_source = resolve_git_source(config.plugin.source, base_dir=plugin_dir)
    else:
        assert isinstance(config.plugin.source, ConfigOnlyPluginSource)
        locked_source = LockedConfigOnlyPluginSource()

    action: Literal["resolved", "up-to-date"]
    if lock is None or locked_source != lock.plugin.locked_source:
        action = "resolved"
        write_json_file(
            value=RootLockedPluginConfig.from_locked_source(locked_source),
            file=lock_file,
        )
    else:
        action = "up-to-date"
    return LockResult(plugin_dir=plugin_dir, locked_source=locked_source, action=action)
