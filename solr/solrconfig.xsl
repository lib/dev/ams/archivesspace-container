<?xml version="1.0"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:mode on-no-match="shallow-copy" />

    <!-- Start with luceneMatchVersion set to the lucene version used in the
         image. This only takes effect when the index core is first created,
         as this config is copied into the core at creation and not
         automatically updated. -->
    <xsl:template match="/config/luceneMatchVersion">
        <xsl:copy><xsl:value-of select="environment-variable('LUCENE_VERSION')"/></xsl:copy>
    </xsl:template>

    <!-- Delete <lib> entries. The archivesspace config tries to match some ICU
         jars, which is brittle as the paths vary. Instead we enable the
         analysis-extras solr module, which contains the needed jars. -->
    <xsl:template match="/config/lib"/>

    <!-- Delete cache configs. The archivesspace config uses legacy cache
         classes removed in Solr 9. The solr 9 cache defaults seem fine.-->
    <xsl:template match="/config/query/*[ends-with(name(), 'Cache')]"/>
</xsl:stylesheet>
