ARG SOLR_VERSION

FROM solr:${SOLR_VERSION:?} AS xslt
SHELL [ "/usr/bin/bash", "-euo", "pipefail", "-c" ]
USER root
RUN apt-get update && apt-get install -y libsaxonhe-java
ENV CLASSPATH=/usr/share/java/Saxon-HE.jar


FROM xslt AS archivesspace-solr-config
COPY --from=archivesspace-repo /solr /solr-conf-src
COPY *.xsl /config-transforms/
WORKDIR /solr-conf
RUN ls -l /solr-conf-src/
RUN cp -a /solr-conf-src/*.{xml,txt} .
RUN <<EOF
LUCENE_VERSION=$(\
    find -L /opt/solr -name 'lucene-core*.jar' -exec basename {} \; \
    | grep -P --only-matching '\d+\.\d+\.\d+'
)
export LUCENE_VERSION=${LUCENE_VERSION:?}
java net.sf.saxon.Transform -xsl:/config-transforms/solrconfig.xsl \
    -s:/solr-conf-src/solrconfig.xml > solrconfig.xml
EOF


FROM solr:${SOLR_VERSION:?} AS archivesspace-solr
USER root

# archivesspace expects Lucene's ICU4j integration to be enabled. It's in the
# analysis-extras module.
ENV SOLR_MODULES=analysis-extras

SHELL [ "/usr/bin/bash", "-euo", "pipefail", "-c" ]
RUN --mount=from=archivesspace-solr-config,source=/solr-conf,target=/solr-conf \
    mkdir -p /opt/solr/server/solr/configsets/archivesspace/conf \
    && cp -a /solr-conf/* \
             /opt/solr/server/solr/configsets/archivesspace/conf/
USER solr
# When running the image without args, auto-create the archivesspace index core
# and start solr.
CMD ["solr-precreate", "archivesspace", "/opt/solr/server/solr/configsets/archivesspace"]
