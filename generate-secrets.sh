#!/usr/bin/env bash
set -euo pipefail
cd "$(dirname "${BASH_SOURCE[0]:?}")"

secrets=(
    mariadb_root_password
    mariadb_archivesspace_password
    archivesspace_search_user_secret
    archivesspace_public_user_secret
    archivesspace_frontend_user_secret
    archivesspace_public_cookie_secret
    archivesspace_frontend_cookie_secret
)

mkdir -p secrets.d
for secret in "${secrets[@]:?}"; do
    secret_file="secrets.d/${secret:?}"
    if [[ ! -e "${secret_file:?}" ]]; then
        secret_value=$(head -c 128 /dev/random | sha256sum | cut -c 1-20)
        echo -n "${secret_value:?}" > "${secret_file:?}"
        echo "Created secret file ${secret_file:?}" >&2
    fi
done
