ARCHIVESSPACE_VERSION = "v3.5.0"
SOLR_VERSION = "9.3"

variable "ARCHIVESSPACE_REPO_URL" {
    default = "https://github.com/archivesspace/archivesspace.git"
}

variable "ARCHIVESSPACE_REPO_REV" {
    default = ARCHIVESSPACE_VERSION
}

variable "ARCHIVESSPACE_REPO" {
    default = "${ARCHIVESSPACE_REPO_URL}#${ARCHIVESSPACE_REPO_REV}"
}

variable "ARCHIVESSPACE_CONTAINER_REPO_PREFIX" {
    default = "registry.gitlab.developers.cam.ac.uk/lib/dev/ams/archivesspace-container"
}

variable "ARCHIVESSPACE_CONTAINER_NAME" {
    default = "archivesspace"
}

variable "ARCHIVESSPACE_CONTAINER_SOLR_NAME" {
    default = "solr-${SOLR_VERSION}"
}

variable "ARCHIVESSPACE_JETTY_VERSION" {
    # Only 9.x is expected to work.
    default = "9.4.56.v20240826"
}

variable "CI_COMMIT_BRANCH" {
    default = "dev"
}

variable "CI_COMMIT_SHA" {
    default = ""
}

variable "CI" {
    default = false
}

variable "CI_INCLUDE_DATE_IN_TAGS" {
    default = CI
}

variable "SSH_AUTH_SOCK" {}

variable "BAKE_PUSH" {
  default = ""
}

variable "ARCHIVESSPACE_BUILD_PLUGIN_DIR" {
  default = "./.empty-dir"
}

variable "ARCHIVESSPACE_SMOKE_TEST_STARTUP_TIMEOUT" {
  default = null
}

group "default" {
    targets = ["archivesspace", "solr"]
}

group "test" {
    targets = ["archivesspace-smoke-test"]
}

function "pushable_image_output" {
    params = []
    result = BAKE_PUSH == "true" ? ["type=registry"] : []
}

function "tag-date" {
    params = []
    result = formatdate("YYYY.MM.DD", timestamp())
}

function "with-tag-date" {
    params = [base_tag]
    result = CI_INCLUDE_DATE_IN_TAGS ? [base_tag, "${base_tag}-${tag-date()}"] : [base_tag]
}

function "tags" {
    params = [branch]
    result = flatten([flatten([
        branch == "main" ? flatten(["latest", with-tag-date("${ARCHIVESSPACE_VERSION}")]) : [],

        try(regex("^[a-z][\\w-]*$", branch), false) == false ? ["dev"] : flatten([
            with-tag-date("${ARCHIVESSPACE_VERSION}-branch-${branch}"),
            with-tag-date("branch-${branch}"),
        ])
    ])])
}

function "archivesspace-tags" {
    params = []
    result = formatlist("${ARCHIVESSPACE_CONTAINER_REPO_PREFIX}/${ARCHIVESSPACE_CONTAINER_NAME}:%s", tags(CI_COMMIT_BRANCH))
}

function "solr-tags" {
    params = []
    result = formatlist("${ARCHIVESSPACE_CONTAINER_REPO_PREFIX}/${ARCHIVESSPACE_CONTAINER_SOLR_NAME}:%s", tags(CI_COMMIT_BRANCH))
}

target "archivesspace" {
    context = "archivesspace"
    target = "archivesspace"
    contexts = {
        archivesspace-repo = ARCHIVESSPACE_REPO
        aspace-plugins-cli = "./tools/aspace_plugins/"
        archivesspace-plugins = ARCHIVESSPACE_BUILD_PLUGIN_DIR
    }
    args = {
        ARCHIVESSPACE_VERSION = ARCHIVESSPACE_VERSION
        ARCHIVESSPACE_CONTAINER_BUILD_SHA = CI_COMMIT_SHA
        JETTY_VERSION = ARCHIVESSPACE_JETTY_VERSION
    }
    tags = archivesspace-tags()
    ssh = ["default=${SSH_AUTH_SOCK}"]
    output = pushable_image_output()
}

target "archivesspace-prebuild" {
    inherits = ["archivesspace"]
    target = "archivesspace-release"
    tags = ["archivesspace-prebuild"]
}

target "archivesspace-plugin-files" {
    inherits = ["archivesspace"]
    target = "plugin-files"
    tags = ["plugin-files"]
}

target "__no_push" {
    tags = []
    output = ["type=cacheonly"]
}

target "archivesspace-smoke-test" {
    inherits = ["archivesspace", "__no_push"]
    target = "archivesspace-smoke-test"
    no-cache-filter = ["archivesspace-smoke-test"]
    args = {
        ARCHIVESSPACE_SMOKE_TEST_STARTUP_TIMEOUT = ARCHIVESSPACE_SMOKE_TEST_STARTUP_TIMEOUT
    }
}

target "solr" {
    context = "solr"
    target = "archivesspace-solr"
    contexts = {
        archivesspace-repo = "https://github.com/archivesspace/archivesspace.git#${ARCHIVESSPACE_VERSION}"
    }
    args = {
        SOLR_VERSION = SOLR_VERSION
    }
    tags = solr-tags()
    output = pushable_image_output()
}
