# ArchivesSpace Container Images

[Cambridge AMS]: https://archivesearch.lib.cam.ac.uk/
[ArchivesSpace]: https://archivesspace.org/
[official ArchivesSpace image]:
  https://hub.docker.com/r/archivesspace/archivesspace/

This repo defines a container image for [ArchivesSpace]. We use it at Cambridge
University to deploy our instance of ArchivesSpace. It's different to the
[official ArchivesSpace image] in a few ways:

- It can run ArchivesSpace components (backend, frontend, indexer, etc) in
  isolation, or all at once one.
  - This allows ArchivesSpace to be deployed as a collection of containers, each
    hosting one component
  - Multiple instances of each component can run in parallel for performance,
    reliability, and zero-downtime deployment updates
- Plugins are baked into the image at build time
  - The repo supports configuring plugins at build time, to create a customised
    image.
- The image uses environment variables for configuration, and supports the
  `_FILE` suffix envar secret pattern for secrets.
- To allow container orchestrators to manage ArchivesSpace containers
  effectively:
  - The ArchivesSpace app is configured to terminate if it encounters an error
    at startup.
  - Healthchecks to ensure all the expected app components are working.
  - Logs go to stdout/stderr streams

## Using

There's a docker compose project in this repo that demonstrates the image. See
the [Docker Compose](#docker-compose) section.

See [archivesspace/README.md](archivesspace/README.md) for instructions for
running the image, supported environment variables, etc.

## Building the image

[docker buildx bake]: https://docs.docker.com/build/bake/

We use a [docker buildx bake] file to configure the image build. To build, run:

```console
# build the archivesspace and solr images
$ docker buildx bake

# Show the build configuration without actually building. Useful to understand
# what is being built, especially when setting environment variables to
# configure the build.
$ docker buildx bake --print

# Test the archivesspace image to check it can start the ArchivesSpace app and
# pass healthchecks. (We run this in CI before pushing to check that a build is
# functional.)
$ docker buildx bake archivesspace-smoke-test
```

By default, the build does not include any ArchivesSpace plugins. To build an
image with plugins, create a plugin configuration dir as described below, and
run the build, using the `ARCHIVESSPACE_BUILD_PLUGIN_DIR` envar to point to your
config dir:

```console
$ ARCHIVESSPACE_BUILD_PLUGIN_DIR=../archivesspace-plugins \
  ARCHIVESSPACE_CONTAINER_REPO_PREFIX=myorganisation \
  docker buildx bake archivesspace
[+] Building 67.8s (45/45) FINISHED
 => [internal] load build definition from Dockerfile
 ...
 => => writing image sha256:45cda431d2a3dd8da5de5b3b0a110e4923fd4d17580695202762556a779a32f3
 => => naming to docker.io/myorganisation/archivesspace:v3.4.1-branch-dev
 => => naming to docker.io/myorganisation/archivesspace:branch-dev
```

## Plugin configuration

To create an image with plugins, you need to create some config files to
describe the plugins you need, and rebuild the image as described above.

> This is how we deploy the Cambridge University ArchivesSpace instance and you
> can find our plugin configuration
> [here](https://gitlab.developers.cam.ac.uk/lib/dev/ams/infra) as an example.

1. Create a directory called `archivesspace-plugins`
1. Create subdirectories for each plugin you need (the installed plugin will use
   the same directory name).
1. In each plugin directory, create a file named `plugin.toml` with this
   template as a guide:

```toml
[plugin]
# If the plugin needs to write to files at runtime, you must list them here. By
# default, all files in the built image are not writable by the archivesspace
# user. Paths starting with a / are relative to the ArchivesSpace install dir.
# Paths without a / are relative to the plugin's installed dir.
# writable_files = ["frontend/assets/colour_theme.css"]

# The load order of plugins is defined by their ordering_priority value. Default
# is 50 if not set. Lower values load earlier.
# ordering_priority = 50

# If the plugin needs to overwrite archivesspace files, list them here. The src
# paths are relative to the plugin's installed dir. The dst paths are relative
# to the ArchivesSpace install dir.
# global_file_overrides = [
#     { src = "stylesheets/as-ead-pdf.xsl", dst = "stylesheets/as-ead-pdf.xsl" },
# ]

# Where the plugin's files come from when installing during the build
[plugin.source]
# "git" sources fetch plugin files from a specific commit of a git repo. This is
# the only supported method of fetching plugin files currently.
# Plugins that only need to add a config.rb or docker-entrypoint.sh file can use
# type "config-only" with no other source properties.
type = "git"

# The URL of the git repo, either as HTTPS or SSH
repo = "https://github.com/lyrasis/aspace-oauth.git"
# repo = "git@github.com:lyrasis/aspace-oauth.git"

# The reference name (e.g. branch, tag, commit hash) to check out
ref = "master"

# When using SSH git repos, you need to provide SSH key fingerprints for the SSH
# server, for git to securly connect to the SSH server when cloning. Not
# neccisary for HTTPS git repos.
extra_ssh_known_hosts_lines = [
  # https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/githubs-ssh-key-fingerprints
  "github.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl",
  "github.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEmKSENjQEezOmxkZMy7opKgwFB9nkt5YRrYMjNuG5N87uRgg6CLrbo5wAdT/y6v0mKV0U2w0WZ2YB/++Tpockg=",
  "github.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCj7ndNxQowgcQnjshcLrqPEiiphnt+VTTvDP6mHBL9j1aNUkY4Ue1gvwnGLVlOhGeYrnZaMgRK6+PKCUXaDbC7qtbW8gIkhL7aGCsOr/C56SJMy/BCZfxd1nWzAOxSDPgVsmerOBYfNqltV9/hWCqBywINIR+5dIg6JTJ72pcEpEjcYgXkE2YEFXV1JHnsKgbLWNlhScqb2UmyRkQyytRLtL+38TGxkxCflmO+5Z8CSSNY7GidjMIZ7Q4zMjA2n1nGrlTDkzwDCsw+wqFPGQA179cnfGWOWRVruj16z6XyvxvjJwbz0wQZ75XK5tKSb7FNyeIEs4TT4jk+S4dhPeAUC5y+bDYirYgM4GC7uEnztnZyaVWQ7B381AK4Qdrwt51ZqExKbQpTUNn+EjqoTwvqNj4kqx5QUCI0ThS/YkOxJCXmPUWZbhjpCg56i+2aB6CmK2JGhn57K5mj0MNdBXA4/WnwH6XoPWJzK5Nyu2zB3nAZp+S5hpQs+p1vN1/wsjk=",
]
```

1. Lock the plugin source to a fixed git commit hash to ensure the build is
   repeatable.

   We have an ArchivesSpace plugin tool to lock versions and install plugins
   from these config files. It's written in Python, so it needs a bit of setup
   to run:

   1. Install pipx (which is a Python tool that installs/manages other Python
      tools): https://pypa.github.io/pipx/installation/
   1. Install the `aspace-plugins` tool. From this repository, in a terminal,
      run: `pipx install ./tools/aspace_plugins`

      You should now be able to run the `aspace-plugins` program in your
      terminal. You can uninstall it by running `pipx uninstall aspace-plugins`.

   1. Lock your plugin configs by running:
      `aspace-plugins lock ./archivesspace-plugins/*` (i.e. give it the path of
      each plugin config dir as an argument). It should look something like
      this:

      ```console
      $ aspace-plugins lock ./archivesspace-plugins/*
      50_aspace_datadog: up-to-date: git@gitlab.developers.cam.ac.uk:lib/dev/ams/aspace-datadog.git#log-scope -> 417a19a10b3425b2e9de5c32fd561f4380d7e351
      50_aspace_oauth: up-to-date: git@gitlab.developers.cam.ac.uk:lib/dev/ams/aspace-oauth.git#replace-tmp-file-with-signed-token -> c9604ae71fe0abe6e196e619ff4482a146cb9bc2
      ```

      In each plugin config dir you'll now have a `plugin.lock` file that looks
      like this:

      ```json
      {
        "plugin": {
          "locked_source": {
            "repo": "git@gitlab.developers.cam.ac.uk:lib/dev/ams/aspace-datadog.git",
            "ref": "log-scope",
            "resolved_ref": "417a19a10b3425b2e9de5c32fd561f4380d7e351"
          }
        }
      }
      ```

      This means the plugin will always install from the indicated git commit
      hash. You should commit these lock files to version control, and re-run
      `aspace-plugins lock` when you need to update to a new commit in the
      source repo.

### Plugin runtime configuration

If a plugin needs to customise ArchivesSpace `AppConfig` values, you can create
a `config.rb` file in the plugin config dir. The image build will install this
file and it will be run by the container's main `config.rb` automatically.

See the container's own `config.rb` to see how sets `AppConfig` values from
environment variables — you can use the same functions in your own `config.rb`.

### Plugin container entrypoint

Plugins can perform container startup actions by creating a
`docker-entrypoint.sh` file in the plugin config dir. The container's own
entrypoint will automatically invoke the plugin's entrypoint just before
starting the ArchivesSpace app.

The working directory will be the plugin's install dir. If the plugin entrypoint
exits with a non-zero status, the container will exit without running
ArchivesSpace.

## Docker Compose

There's a docker compose project in this repo that runs ArchivesSpace using the
image. Follow these steps to run it.

```console
# If you're using a devcontainer, compose can't bind mount files for docker
# secrets. To work around this we can give compose secrets using environment
# variables. The .envrc file here enables this behaviour. Install direnv and
# allow the .envrc file to activate
$ direnv allow .
```

Non-devcontainer users can start here:

```console
# Generate random secret values for the various ArchivesSpace secrets
$ ./generate-secrets.sh

# Run the db migrations to initialise the database
$ docker compose run archivesspace-backend run-script setup-database.sh

# Start all the components. Because there are a lot of containers, the combined
# log output is hard to follow, so start with -d to avoid seeing all the logs.
# Use docker compose logs -f <service> to monitor individual components.

$ docker compose up -d

# You can also run ArchivesSpace as a single container, which uses less memory.
$ docker compose --profile unified up -d

# Also need to use the profile when shutting down.
$ docker compose --profile unified down
```

The components will be available on these URLs once everything's started:

| Component        | URL                   |
| ---------------- | --------------------- |
| Staff interface  | http://localhost:8080 |
| Public interface | http://localhost:8081 |
| OAI interface    | http://localhost:8082 |
| Solr             | http://localhost:8983 |
