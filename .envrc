# shellcheck shell=bash

# docker compose doesn't support docker secrets and can't bind mount secrets
# from a container volume, so in a dev container we have to use environment
# variables to define secrets. (Which kind of defeats the point of secrets, but
# these are just for local development env anyway.)
watch_file secrets.d
if [[ -d secrets.d ]]; then
    readarray -t secrets < <(find secrets.d/ -maxdepth 1 -type f -exec basename {} \;)
    for secret in "${secrets[@]}"; do
        declare -n secret_var="DOCKER_COMPOSE_SECRET_${secret^^}"
        # shellcheck disable=SC2034
        secret_var=$(cat "secrets.d/${secret:?}")
        export "DOCKER_COMPOSE_SECRET_${secret^^}"
    done
fi

COMPOSE_FILE="$(pwd)/docker-compose.yml:$(pwd)/docker-compose.environment-secrets.yml"
export COMPOSE_FILE

# run .envrc from a parent dir if one exists. This allows a plugin config repo
# that includes this repo as a submodule to contribute envars here.
source_up_if_exists
