# Cambridge University ArchivesSpace Container Image

[Cambridge University's ArchivesSpace]: https://archivesearch.lib.cam.ac.uk/

This directory contains files used to build a container image for ArchivesSpace.
This image (with some plugins added) is used to run [Cambridge University's
ArchivesSpace].

It's published to our gitlab registry as:

- [`registry.gitlab.developers.cam.ac.uk/lib/dev/ams/archivesspace-container/archivesspace`](https://gitlab.developers.cam.ac.uk/lib/dev/ams/archivesspace-container/archivesspace/container_registry)

Tags follow the ArchivesSpace version, with an optional branch suffix:

- `:latest` — the most-recent build from the `main` branch, this is the
  most-recent production build.
- `:3.4.1` — The most recent production build of ArchivesSpace 3.4.1.
- `:3.4.1-branch-dev` — The most recent build containing 3.4.1 from the `dev`
  branch.
- `:branch-dev` — The most recent build from the `dev` branch.

## Usage

```
# These examples assume a shorthand tag for the image:
$ docker image tag registry.gitlab.developers.cam.ac.uk/lib/dev/ams/archivesspace-container/archivesspace aspace
```

Run the image with `--help` to see the available commands.

```
$ docker run --rm aspace --help
```

The image has 3 modes of operation:

1. Run servers one or more ArchivesSpace components:
   - `docker run --rm aspace run-server backend frontend`
   - `docker run --rm aspace run-server all`
2. Run one of the management scripts (from `archivesspace/scripts/*.sh`):
   - `docker run --rm aspace run-script setup-database.sh`
3. Run an arbitrary command, e.g. get a shell in the container:
   - `docker run --rm -it aspace bash`

## Configuration

The image uses environment variables to configure ArchivesSpace. They take the
form `ASPACE_*`, e.g. `ASPACE_DATABASE_PASSWORD`. Values can come from files,
e.g. to use Docker secrets: `ASPACE_DATABASE_PASSWORD_FILE=/tmp/foo` will read
the password from `/tmp/foo`.

### Secrets

Secrets used in the configuration should be random password-like tokens with
enough entropy to not be brute-forcible (say, at least 128 bits). For example,
using [pwgen](https://packages.debian.org/bookworm/pwgen): `$ pwgen --secure 40`

### Main options

| Required By       | Name                            | Description                                                                                                                              | Default         |
| ----------------- | ------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- | --------------- |
| all               | `ASPACE_BACKEND_URL`            | The internal URL components use when making HTTP requests to the backend. This also defines the port that the backend server listens on. |                 |
| frontend          | `ASPACE_FRONTEND_URL`           | Defines the port that the frontend server listens on.                                                                                    |                 |
| public            | `ASPACE_PUBLIC_URL`             | Defines the port that the public server listens on.                                                                                      |                 |
| oai               | `ASPACE_OAI_URL`                | Defines the port that the OAI server listens on.                                                                                         |                 |
| backend, indexer  | `ASPACE_SOLR_URL`               | The internal URL components use when making HTTP requests to the solr search server.                                                     |                 |
| indexer           | `ASPACE_INDEXER_URL`            | Defines the port that the indexer server listens on.                                                                                     |                 |
| docs              | `ASPACE_DOCS_URL`               | Defines the port that the docs server listens on.                                                                                        |                 |
| frontend, backend | `ASPACE_FRONTEND_PROXY_URL`     | The public URL of the frontend server.                                                                                                   |                 |
| public, backend   | `ASPACE_PUBLIC_PROXY_URL`       | The public URL of the public server.                                                                                                     |                 |
| oai, backend      | `ASPACE_OAI_PROXY_URL`          | The public URL of the OAI server.                                                                                                        |                 |
| indexer           | `ASPACE_SEARCH_USER_SECRET`     | Used by the indexer to authenticate to the backend.                                                                                      |                 |
| public            | `ASPACE_PUBLIC_USER_SECRET`     | Used by the public server to authenticate to the backend.                                                                                |                 |
| frontend          | `ASPACE_FRONTEND_USER_SECRET`   | Used by the frontend server to authenticate to the backend.                                                                              |                 |
| frontend          | `ASPACE_FRONTEND_COOKIE_SECRET` | Used by the frontend server to validate encrypted/signed cookies it creates.                                                             |                 |
| public            | `ASPACE_PUBLIC_COOKIE_SECRET`   | Used by the public server to validate encrypted/signed cookies it creates.                                                               |                 |
| [backend]         | `ASPACE_DATABASE_HOSTNAME`      | Hostname of the mysql-compatible database server                                                                                         | `localhost`     |
| [backend]         | `ASPACE_DATABASE_PORT`          | Port number of the mysql-compatible database server                                                                                      | `3306`          |
| [backend]         | `ASPACE_DATABASE_NAME`          | Name of the database on the mysql-compatible database server                                                                             | `archivesspace` |
| [backend]         | `ASPACE_DATABASE_USER`          | Username to authenticate to the mysql-compatible database server with                                                                    | `archivesspace` |
| backend           | `ASPACE_DATABASE_PASSWORD`      | Password to authenticate to the mysql-compatible database server with                                                                    |                 |
| [backend]         | `ASPACE_DATABASE_SSL`           | Port number of the mysql-compatible database server                                                                                      | `3306`          |

### Additional options

| Required By | Name                                    | Description                                                                                                                                             | Default |
| ----------- | --------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- |
|             | `ASPACE_SHOW_CONFIG`                    | If `true`, server components will log their configuration to the container's stdout on startup. Useful to debug configuration problems.                 | `false` |
|             | `ASPACE_LOG_LEVEL`                      | Log messages below this level will not be printed (`error`, `warn`, `info`, `debug`).                                                                   | `warn`  |
|             | `ASPACE_DATA_DIR`                       | The filesystem path of the directory that `archivesspace` user will write persistent state to.                                                          |         |
|             | `ASPACE_DEFAULT_ADMIN_PASSWORD`         | Password for the default admin user, only used when the database is empty.                                                                              |         |
|             | `ASPACE_PLUGIN_DISABLE`                 | Don't enable plugins whose directory name matches this Ruby regular expression. Empty string means nothing is disabled.                                 |         |
|             | `ASPACE_PLUGIN_DISABLE_{name}`          | Don't enable the plugin corresponding to `{name}`, which is the uppercase plugin name with `_` instead of `-`.                                          | false   |
| [indexer]   | `ASPACE_INDEXER_RECORDS_PER_THREAD`     | Same as `AppConfig[:indexer_records_per_thread]`                                                                                                        | 50      |
| [indexer]   | `ASPACE_INDEXER_THREAD_COUNT`           | Same as `AppConfig[:indexer_thread_count]`                                                                                                              | 2       |
| [indexer]   | `ASPACE_INDEXER_SOLR_TIMEOUT_SECONDS`   | Same as `AppConfig[:indexer_solr_timeout_seconds]`                                                                                                      | 1200    |
| [indexer]   | `ASPACE_PUI_INDEXER_RECORDS_PER_THREAD` | Same as `AppConfig[:pui_indexer_records_per_thread]`                                                                                                    | 50      |
| [indexer]   | `ASPACE_PUI_INDEXER_THREAD_COUNT`       | Same as `AppConfig[:pui_indexer_thread_count]`                                                                                                          | 2       |
| [indexer]   | `ASPACE_BACKEND_INSTANCE_URLS`          | Whitespace-separated URLs of each backend server instance the indexer should watch for changes. Only necessary when 2 or more backend servers are used. |         |

[`AppConfig.load_overrides_from_environment()`]:
  https://github.com/archivesspace/archivesspace/blob/a81ea29ea0167374ef7d38e8a5631627b9cd1d2d/common/config/config-distribution.rb#L93

You can add extra config envars by building the image with plugin config dir
containing a `config.rb` file. See
[Plugin runtime configuration](../README.md#plugin-runtime-configuration)
section of this repo's README.

Also, ArchivesSpace itself can load settings from environment variables (via
`APPCONFIG_*`, [`AppConfig.load_overrides_from_environment()`]), so it's
possible to set arbitrary config values without rebuilding the image. But
`APPCONFIG_` envars are not validated and are less flexible than our `ASPACE_`
envars, so it's recommended to use the `config.rb` approach so that your
containers can detect configuration errors at startup and raise an error.

Best to add options we actively use to config.rb though.

## Container directories

The container stores persistent data at `/var/archivesspace`. This directory
should be mounted from a persistent volume.

The ArchivesSpace application files are at `/opt/archivesspace` and are mostly
read-only. Plugins are able to configure specific files to be writeable.

Temporary data is at `/var/tmp/archivesspace`. This doesn't need to be mounted
as tmpfs. The container also stores ephemeral docker-healthcheck-related files
in `/tmp`.

The image currently logs JVM GC activity under `/var/log/archivesspace` to help
tune JVM memory parameters. The log files are rotated and auto-deleted. This can
be disabled by setting `ASPACE_JAVA_GC_LOG_OPTS` to a non-empty string, like a
space.
