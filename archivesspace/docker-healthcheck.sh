#!/usr/bin/env bash
set -euo pipefail

healthcheck_response_deadline_seconds=20
healthcheck_ping_response_deadline_seconds=5
healthcheck_dir="/tmp/docker-healthchecks"
components_file="${healthcheck_dir:?}/components-running"

# We use docker swarm to manage our ArchivesSpace containers, and we assign
# distinct hostnames to backend instances with swarm's template placeholder
# feature "archivesspace-backend-{{.Task.Slot}}", giving hostnames with -1 -2 -3
# etc suffixes. We've occasionally seen indexer containers loose the ability to
# resolve individual backend instance hostnames, despite the instances being up
# and healthy. Re-deploying the indexer container fixes this. So as a
# workaround, we fail the indexer's healthcheck if it can't access all of its
# backend instances.
function check_indexer_can_access_backend_instances() {
  # ASPACE_BACKEND_INSTANCE_URLS is a whitespace-separated set of http URLs. It
  # will be empty/not set unless multiple backends are being used.
  read -r -a instances < <(tr '\n' ' ' <<<"${ASPACE_BACKEND_INSTANCE_URLS:-}") \
    || true  # read returns 1 on EOF

  for instance in "${instances[@]}"; do
    if [[ $instance  =~ ^https?://([a-zA-Z0-9._-]+) ]]; then
      local hostname=${BASH_REMATCH[1]}
    else
      echo "Error: could not extract hostname from ASPACE_BACKEND_INSTANCE_URLS url: ${instance@Q}" >&2
      return 1
    fi

    ping_status=0
    ping_output=$(ping -w "${healthcheck_ping_response_deadline_seconds:?}" -c 1 -- "${hostname:?}" 2>&1) || ping_status=$?

    if [[ ${ping_status:?} != 0 ]]; then
      if [[ "${ping_output:-}" ]]; then echo "${ping_output:?}" >&2; fi
      echo "Unhealthy: failed to ping indexer backend instance ${hostname@Q} (exit status ${ping_status:?})" >&2
      return 2
    fi
  done
}

if [[ ${DOCKER_HEALTHCHECK_DISABLED:-} \
  || (
    -L "${components_file:?}"
    && $(readlink -f "${components_file:?}") == /dev/null
  ) ]]; then
  echo "Healthy: healthchecks are disabled"
  exit
fi

if [[ ! -f "${components_file:?}" ]]; then
  echo "Error: healthcheck components file does not exist" >&2
  exit 1
fi

readarray -t components < "${components_file:?}"

components_checked=0
for c in "${components[@]}"; do
  port_file="${healthcheck_dir:?}/${c:?}-port"

  if [[ ! -f "${port_file:?}" ]]; then
    echo "Unhealthy: component ${c:?} is expected to be running but port file does not exist: ${port_file:?}" >&2
    exit 2
  fi

  port=$(cat "${port_file:?}")

  if [[ ! "$port" =~ ^[1-9][0-9]*$ ]]; then
    echo "Error: component ${c:?} port file does not contain an integer: ${port@Q}" >&2
    exit 1
  fi

  # We consider a component healthy if we can get a response to an HTTP request.
  # We don't interpret the actual response. It may be responding with an error
  # because of a problem with a downstream dependency (e.g. database unavailable)
  # in which case the immediate component is OK — killing its container would
  # not help.
  component_url="http://localhost:${port:?}"
  if ! curl --silent --show-error \
    --max-time "${healthcheck_response_deadline_seconds:?}" \
    --user-agent "docker-healthcheck.sh" \
    "${component_url:?}" > /dev/null
  then
    echo "Unhealthy: unable to communicate with component ${c:?} at ${component_url:?}" >&2
    exit 2
  fi

  # Optionally report unhealthy if an indexer can't access its configured
  # backend instances. Disabled by default.
  if [[ $c == "indexer" && "${DOCKER_HEALTHCHECK_ASPACE_INDEXER_BACKEND_INSTANCE_ACCESSIBLE_ENABLED:-}" ]]; then
    check_indexer_can_access_backend_instances
  fi

  (( components_checked+=1 ))
done

if (( components_checked == 0 )); then
  echo "Error: Healthchecks are enabled but no healthcheck components listed in components file: ${components_file:?}" >&2
  exit 1
fi
