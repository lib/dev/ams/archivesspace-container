require 'securerandom'
require 'set'
require 'uri'
require_relative '../launcher/container_startup'

class SettingNotAvailable < StandardError
end

def get_file_env(name)
  path = ENV.fetch("#{name}_FILE", nil)
  if path.nil? or path.empty?
    return nil
  end
  return File.read(path)
end

NO_DEFAULT = Object.new

def aspace_env(name, default: NO_DEFAULT, tx: nil)
  envar = "ASPACE_#{name}"
  value = ENV.fetch(envar, nil)

  if value.nil? or value.empty?
    value = get_file_env(envar)
  end

  if value.nil? or value.empty?
    if default.is_a? Proc
      value = default.call
    elsif default != NO_DEFAULT
      value = default
    else
      if "#{value}".empty?
        raise SettingNotAvailable.new("environment variable #{envar} is empty")
      end
      raise SettingNotAvailable.new("environment variable #{envar} is not set")
    end
  end

  if tx.nil?
    return value
  else
    return tx.call(value)
  end
end

def optional_setting(config_key, name, default: nil)
  begin
    AppConfig[config_key] = aspace_env(name, default: default)
  rescue SettingNotAvailable
    # do nothing
  end
end

def setting(config_key, name, default: nil, required_by: :all, tx: nil)
  servers = Set[:backend, :frontend, :public, :indexer, :docs, :oai]
  if required_by == :all
    required_by = servers
  elsif required_by.is_a? Symbol
    required_by = Set[required_by]
  elsif required_by.respond_to? :to_a
    required_by = required_by.to_a.to_set
  else
    raise "required_by must be a Symbol or convertable to Array of symbols: #{required_by}"
  end
  if (required_by - servers).length > 0
    raise "Unknown server name in equired_by: #{required_by - servers}"
  end

  begin
    AppConfig[config_key] = aspace_env(name, default: default, tx: tx)
  rescue SettingNotAvailable => e
    servers_requiring_setting = required_by.dup
    servers_requiring_setting.select! {
      |server| AppConfig["run_#{server}".to_sym]
    }
    if servers_requiring_setting.length > 0
      raise SettingNotAvailable.new("#{e} but is required by server(s) to be run: #{servers_requiring_setting.to_a.sort.join ", "}")
    end
    # if no servers that require the settings are enabled, do nothing
  end
end

def parse_bool(value)
  value == true or value == "true"
end

# Component configuration
AppConfig[:run_backend] = aspace_env("RUN_BACKEND", tx: method(:parse_bool))
AppConfig[:run_frontend] = aspace_env("RUN_FRONTEND", tx: method(:parse_bool))
AppConfig[:run_public] = aspace_env("RUN_PUBLIC", tx: method(:parse_bool))
AppConfig[:run_indexer] = aspace_env("RUN_INDEXER", tx: method(:parse_bool))
AppConfig[:run_docs] = aspace_env("RUN_DOCS", default: false, tx: method(:parse_bool))
AppConfig[:run_oai] = aspace_env("RUN_OAI", tx: method(:parse_bool))

# Note: these are different to the default enable_#{component} settings
AppConfig[:backend_enabled] = aspace_env("ENABLE_BACKEND", default: true, tx: method(:parse_bool))
AppConfig[:frontend_enabled] = aspace_env("ENABLE_FRONTEND", default: true, tx: method(:parse_bool))
AppConfig[:public_enabled] = aspace_env("ENABLE_PUBLIC", default: true, tx: method(:parse_bool))
AppConfig[:indexer_enabled] = aspace_env("ENABLE_INDEXER", default: true, tx: method(:parse_bool))
AppConfig[:docs_enabled] = aspace_env("ENABLE_DOCS", default: true, tx: method(:parse_bool))
AppConfig[:oai_enabled] = aspace_env("ENABLE_OAI", default: true, tx: method(:parse_bool))

# See container_startup.rb for more on this.
# The :enable_xxx options need respond differently in the launcher vs the rest.
for component in ['backend', 'frontend', 'public', 'indexer', 'docs', 'oai']
  AppConfig["enable_#{component}".to_sym] = ->(component) {
    return ->() { ArchivesSpace::ContainerWorkarounds.is_component_enabled_considering_calling_thread?(component) }
  }.call(component)
end

setting(:backend_url, "BACKEND_URL", required_by: :all)
setting(:frontend_url, "FRONTEND_URL", required_by: :frontend)
setting(:public_url, "PUBLIC_URL", required_by: :public)
setting(:oai_url, "OAI_URL", required_by: :oai)
setting(:solr_url, "SOLR_URL", required_by: [:backend, :indexer])
setting(:indexer_url, "INDEXER_URL", required_by: :indexer)
setting(:docs_url, "DOCS_URL", required_by: :docs)

# ASPACE_DATA_DIR has a default in the Dockerfile but can be overridden
AppConfig[:data_directory] = aspace_env("DATA_DIR")

# Solr is external, and we can't expect its config to be present in the
# archivesspace container image in identical form to the config in the solr
# container.
AppConfig[:solr_verify_checksums] = false

# Admin configuration
optional_setting(:default_admin_password, "DEFAULT_ADMIN_PASSWORD")


# Passwords
setting(:search_user_secret, "SEARCH_USER_SECRET", required_by: :indexer)
setting(:public_user_secret, "PUBLIC_USER_SECRET", required_by: :public)
# frontend and staff are synonymous
setting(:staff_user_secret, "FRONTEND_USER_SECRET", required_by: :frontend)

setting(:frontend_cookie_secret, "FRONTEND_COOKIE_SECRET", required_by: :frontend)
setting(:public_cookie_secret, "PUBLIC_COOKIE_SECRET", required_by: :public)

# The frontend and public cookie secrets are not used by other components, but
# the launcher still generates secret files for other components if they're not
# set. To avoid generating these files, we generate random values, that will not
# be used for anything.
if not AppConfig[:enable_frontend] and not AppConfig.has_key? :frontend_cookie_secret
  AppConfig[:frontend_cookie_secret] = SecureRandom.hex
end
if not AppConfig[:enable_public] and not AppConfig.has_key? :public_cookie_secret
  AppConfig[:public_cookie_secret] = SecureRandom.hex
end


# Database settings
db_url = URI("mysql://")
db_url.hostname = aspace_env('DATABASE_HOSTNAME', default: 'localhost')
db_url.port = aspace_env('DATABASE_PORT', default: '3306')
db_url.path = "/#{aspace_env('DATABASE_NAME', default: 'archivesspace')}"
db_url.query = {
  "user" => aspace_env('DATABASE_USER', default: 'archivesspace'),
  "password" => aspace_env(
    'DATABASE_PASSWORD',
    default: AppConfig[:run_backend] ? NO_DEFAULT : ''
  ),
  "useUnicode" => "true",
  "characterEncoding" => "UTF-8",
  "useSSL" => String(aspace_env('DATABASE_SSL', default: true, tx: method(:parse_bool)))
}.collect {|k,v| "#{k}=#{URI.escape(v)}" }.join('&')

AppConfig[:db_url] = "jdbc:#{db_url}"

# Logging
log_level = aspace_env("LOG_LEVEL", default: "warn")
AppConfig[:backend_log_level] = log_level
AppConfig[:frontend_log_level] = log_level
AppConfig[:pui_log_level] = log_level
AppConfig[:indexer_log_level] = log_level


# Plugins
plugin_dir = File.join(ENV['ASPACE_LAUNCHER_BASE'], "plugins")
disabled_plugins_regex = aspace_env(
  "PLUGIN_DISABLE", default: nil, tx: ->(r) { r.nil? ? nil : Regexp.new(r) }
)

is_disabled_plugin_name = ->(plugin_name) {
  # Plugins can be disabled in two ways - by matching a regex, or via a specific
  # envar, like ASPACE_PLUGIN_DISABLE_MY_PLUGIN=true (for a plugin named my-plugin).
  # We have two ways because they're suited to different disabling usecases.
  # The regex method is useful to manually disable a bunch of things, e.g.
  # ASPACE_DISABLED_PLUGINS_REGEX=.* whereas the specific envar method is more
  # suited to cases where you need to toggle plugins on/off automatically from
  # code configuring a deployment.
  unless disabled_plugins_regex.nil?
    return true if disabled_plugins_regex.match?(plugin_name)
  end
  return aspace_env(
    "PLUGIN_DISABLE_#{plugin_name.upcase.gsub("-", "_")}",
    default: false, tx: method(:parse_bool)
  )
}

plugins = Hash[Dir.children(plugin_dir).map { |plugin_name|
  order_file = File.join(plugin_dir, plugin_name, "container-config/ordering-priority")
  order = File.exist?(order_file) ? Integer(IO.read(order_file).strip) : 50
  [plugin_name, {
    :disabled => is_disabled_plugin_name.call(plugin_name),
    :ordering_priority => order
  }]
}]

# Sort plugins according to their ordering priority number.
AppConfig[:plugins] = plugins.keys.sort_by {|p| plugins[p][:ordering_priority] }
AppConfig[:plugins].delete_if {|p| plugins[p][:disabled]}

# Indexer settings
AppConfig[:indexer_records_per_thread] = aspace_env('INDEXER_RECORDS_PER_THREAD', default: 50, tx: method(:Integer))
AppConfig[:indexer_thread_count] = aspace_env('INDEXER_THREAD_COUNT', default: 2, tx: method(:Integer))
AppConfig[:indexer_solr_timeout_seconds] = aspace_env('INDEXER_SOLR_TIMEOUT_SECONDS', default: 1200, tx: method(:Integer))

AppConfig[:pui_indexer_records_per_thread] = aspace_env('PUI_INDEXER_RECORDS_PER_THREAD', default: 50, tx: method(:Integer))
AppConfig[:pui_indexer_thread_count] = aspace_env('PUI_INDEXER_THREAD_COUNT', default: 2, tx: method(:Integer))

setting(
  :backend_instance_urls,
  "BACKEND_INSTANCE_URLS",
  tx: ->(urls) {
    split_urls = urls.split(/\s+/)
    return split_urls unless split_urls.length == 0
    return AppConfig[:backend_url]
  },
  default: '',
  required_by: :indexer,
)

# Proxy configuration
setting(:frontend_proxy_url, "FRONTEND_PROXY_URL", required_by: [:frontend, :backend])
setting(:public_proxy_url, "PUBLIC_PROXY_URL", required_by: [:public, :backend])
setting(:oai_proxy_url, "OAI_PROXY_URL", required_by: [:oai, :backend])

AppConfig[:ams_show_config] = aspace_env('SHOW_CONFIG', default: false, tx: method(:parse_bool))

# Allow plugins to contribute to configuration.
# Iterate a copy the list as plugin configs may change the :plugins list.
for plugin_name in [*AppConfig[:plugins]]
  plugin_config_file = File.join(
    ENV['ASPACE_LAUNCHER_BASE'], "plugins", plugin_name, "container-config/config.rb"
  )
  if File.exist? plugin_config_file
    puts "Loading config.rb for plugin #{plugin_name}"
    load plugin_config_file
  end
end
