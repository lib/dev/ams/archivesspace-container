require 'fileutils'
require_relative 'launcher/container_startup'

class ShutdownOnError
  include Java::java.lang.Thread::UncaughtExceptionHandler
  include Java::org.eclipse.jetty.util.component.LifeCycle::Listener

  def initialize(container_startup)
    @container_startup = container_startup
  end

  def lifeCycleFailure(event, error)
    STDERR.puts "ShutdownOnError.lifeCycleFailure: #{event} #{error}"
    shutdown
  end

  def lifeCycleStopped(event)
    STDERR.puts "ShutdownOnError.lifeCycleStopped: #{event}"
    shutdown
  end

  def lifeCycleStarted(event)
  end

  def lifeCycleStarting(event)
  end

  def lifeCycleStopping(event)
  end

  def uncaughtException(t, e)
    STDERR.puts "Error: an exception was not handled by a thread, shutting down"
    STDERR.puts "Uncaught Exception:\n#{e}"
    shutdown
  end

  def shutdown
    STDERR.puts "ShutdownOnError.shutdown"
    begin
      for server in @container_startup.servers
        server.stop
      end
    ensure
      java.lang.System.exit(1)
    end
  end
end

def get_war_name(webapps)
  war_webapp = webapps.find { |webapp| webapp.has_key? :war }
  match = /(\w+)\.war$/.match(war_webapp[:war]) unless war_webapp.nil?
  return match[1] unless match.nil?
end

def enable_server_healthcheck(name, port)
  # Record the port number of the server for docker-healthcheck.sh to use
  healthcheck_dir = "/tmp/docker-healthchecks"
  FileUtils.mkdir_p healthcheck_dir
  IO.write(File.join(healthcheck_dir, "#{name}-port"), "#{port}")
end

def override_temp_directory
  # The default archivesspace temp dir is inside the data dir. I don't like
  # that, as image-specific code and web assets get unpacked from .war files
  # into the temp dir, and the data dir for us is mounted from an external
  # volume. So with the default configuration, the contaimer image is
  # potentially able to run code from an external volume, rather than from the
  # image itself. That's bad both from a security and reliablility POV. Instead,
  # we make tempdir an isolated location. This could be mounted as tmpfs, but
  # it's also fine to just allow files to be created in the ephemeral container
  # filesystem.
  FileUtils.mkdir_p("/var/tmp/archivesspace")
  java.lang.System.set_property("java.io.tmpdir", "/var/tmp/archivesspace")
end

def show_app_config
  if AppConfig[:ams_show_config]
    require 'pp'
    puts "\nAppConfig:"
    pp(AppConfig.dump_sanitized)
  end
end

# We allow plugins to define code to run at startup, in the same way as this
# launcher_rc file. Plugins can already define plugin_init.rb files, but these
# run during startup of the server component itself, where as this runs earlier,
# before Rails has loaded for example.
def run_plugin_startup_hooks
  AppConfig[:plugins].each do |plugin_name|
    startup_file = File.join(ASUtils.plugin_base_directory, plugin_name, 'container-config/startup.rb')
    if File.exist?(startup_file)
      puts "Loading startup.rb for plugin #{startup_file}"
      load startup_file
    end
  end
end

class ContainerStartupBehaviour
  attr_reader :servers

  def initialize
    @servers = []
    @shutdown_handler = ShutdownOnError.new(self)
  end

  def activate
    register_global_uncaught_exception_handler
    add_server_prepare_hook(self.method(:prepare_server))
    override_temp_directory
    register_launcher_thread
    show_app_config
    enable_strict_http_compliance
    run_plugin_startup_hooks
  end

  def register_global_uncaught_exception_handler
    java.lang.Thread.setDefaultUncaughtExceptionHandler(@shutdown_handler)
  end

  def prepare_server(server, port, webapps)
    @servers.append(server)
    server.addLifeCycleListener(@shutdown_handler)
    for handler in server.handler.get_handlers()
      # By default WebAppContext swallows exceptions thrown during start up.
      # We want to throw errors so that we can notice the error, exit and have
      # the container get restarted.
      if handler.instance_of? org.eclipse.jetty.webapp.WebAppContext
        puts "Enabling setThrowUnavailableOnStartupException for #{handler}"

        # Have jetty itself throw init errors
        handler.setThrowUnavailableOnStartupException(true)

        # jruby-rack also swallows init exceptions from ruby apps it's hosting,
        # so we need to disable that too. Setting this to false causes
        # jruby-rack to not handle errors.
        # https://github.com/jruby/jruby-rack/blob/1.1-stable/src/main/java/org/jruby/rack/RackServletContextListener.java#L85
        # https://github.com/jruby/jruby-rack/blob/master/src/main/java/org/jruby/rack/DefaultRackConfig.java#L326
        handler.setInitParameter("jruby.rack.error", "false")
      end
      puts "Registering shutdown handler for #{handler}"
      handler.addLifeCycleListener(@shutdown_handler)
    end

    # We need to do this in the prepare hook callback, as ArchivesSpace sets up
    # the default temp dir in between launcher_rc being first called and this
    # hook being invoked.
    override_temp_directory

    name = get_war_name(webapps)

    # The docs server doesn't have a :war
    if not name.nil?
      puts "Server starting for #{name}"
      enable_server_healthcheck(name, port)
    end
  end

  def register_launcher_thread
    ArchivesSpace::ContainerWorkarounds.mark_current_thread_as_launcher_thread
  end
end

def enable_strict_http_compliance
  # The org.eclipse.jetty.server.HttpConnectionFactory uses RFC7230 compliance
  # by default. This does not include some extra compliance options in the
  # RFC7230_NO_AMBIGUOUS_URIS mode, and there are further still compliance
  # options (such as NO_AMBIGUOUS_EMPTY_SEGMENT) which are not enabled by default
  # in any pre-defined constant. Here make RFC7230 enable all compliance options.
  default_compliance = org.eclipse.jetty.http.HttpCompliance::RFC7230
  all_compliance_sections = org.eclipse.jetty.http.HttpComplianceSection.values.to_a
  # Each compliance mode constant has an EnumSet containing the compliance mode
  # it enables. This EnumSet is mutable, so we can add extra compliance sections.
  # https://github.com/jetty/jetty.project/blob/dd2c253fce51827789edaada920a3f1b3485e643/jetty-http/src/main/java/org/eclipse/jetty/http/HttpCompliance.java#L215
  default_compliance.sections.add_all(all_compliance_sections)
  # (This is specific to Jetty 9.x. The HttpCompliance enum was somewhat clunky
  #  in 9.x because it couldn't be changed much without breaking the API.)
  puts "Enabled HttpCompliance sections: #{default_compliance.sections}"
end

ContainerStartupBehaviour.new.activate
