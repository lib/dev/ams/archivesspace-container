#!/usr/bin/env bash
set -euo pipefail

export ASPACE_LAUNCHER_BASE=/opt/archivesspace # archivesspace requires this
cd "${ASPACE_LAUNCHER_BASE:?}"

script_dir="${ASPACE_LAUNCHER_BASE:?}/scripts"
healthcheck_dir="/tmp/docker-healthchecks"
healthcheck_components_file="${healthcheck_dir:?}/components-running"

declare -A components=([backend]=false [frontend]=false [public]=false [indexer]=false [oai]=false)

function print_message() {
    echo "docker-entrypoint.sh:" "$@"
}

function script_names() {
    find scripts/ -maxdepth 1 -type f -perm /u+x -user 0 -exec basename '{}' \;
}

function show_help() {
    scripts=$(script_names)
    readarray -t scripts <<<"${scripts:?}"
    msg="\
Usage:
    docker-entrypoint.sh run-server [<component>]...
    docker-entrypoint.sh run-script <script-name> [ARG...]
    docker-entrypoint.sh <command> [ARG...]
    docker-entrypoint.sh --help

Options:
    <component>
        The ArchivesSpace server component to run in the container.
        'all', 'backend', 'frontend', 'public', 'indexer', or 'oai'.

    <script-name>
        The name of one of the ArchivesSpace management scripts:
        ${scripts[*]@Q}

    <command>
        Run an arbitrary program with arguments, e.g. bash to get a shell in the
        container after setting up the environment.
" >&2
    echo "${msg:?}"
}

function reset_healthchecks() {
    mkdir -p "${healthcheck_dir:?}" \
        && rm -f "${healthcheck_components_file:?}"
}

function disable_healthchecks() {
    mkdir -p "${healthcheck_dir:?}" \
        && ln -sf /dev/null "${healthcheck_components_file:?}"
}

function enable_healthcheck_components() {
    if [[ $# == 0 ]]; then return; fi
    mkdir -p "${healthcheck_dir:?}" || return $?
    if [[ -e "${healthcheck_components_file:?}" && \
         ! -f "${healthcheck_components_file:?}" ]]; then
        rm -f "${healthcheck_components_file:?}" || return $?
    fi

    printf '%s\n' "$@" >> "${healthcheck_components_file:?}"
}

args=("$@")

if [[ ${#args[@]} == 0 ]]; then
    print_message "No command specified." >&2
    echo >&2
    show_help >&2
    exit 1
fi

# Show help for --help
for ((i=0;i<${#args[@]};++i)); do
    if [[ ${args[0]} == "run-script" && $i == 2 ]]; then
        break
    fi
    if [[ "${args[$i]}" =~ ^-h|--help$ ]]; then
        show_help
        exit 0
    fi
done

# run-script command: Run an archivesspace script
if [[ ${1:-} == "run-script" ]]; then
    disable_healthchecks
    script_name=${2:-}
    script=$(PATH="${script_dir:?}" command -v "$2") || {
        print_message "Script not found: ${script_name@Q}" >&2
        exit 1
    }
    args=("$@"); args=("${args[@]:2}")
    exec "${script:?}" "${args[@]}"
fi

if [[ ${1:-} != "run-server" ]]; then
    # arguments are a program to run
    disable_healthchecks
    exec "$@" || exit $?
fi

# The command is run-server
for arg in "${args[@]:1}"; do
    if [[ $arg == "all" ]]; then
        for c in "${!components[@]}"; do
            components["${c:?}"]=true
        done
    elif [[ ${components[$arg]:-} ]]; then
        components["${arg:?}"]=true
    else
        print_message "Unknown component: $arg" >&2
        exit 1
    fi
done

any_component_running=false

# Clear any pre-existing checks
reset_healthchecks
for c in "${!components[@]}"; do
    export "ASPACE_RUN_${c^^}=${components[${c:?}]:?}"
    if [[ ${components["${c:?}"]} == true ]]; then
        any_component_running=true
        enable_healthcheck_components "${c:?}"
        echo "${c:?}" >> "${healthcheck_components_file:?}"
    fi
done

# ArchiveSpace doesn't re-use data it creates in its tmp directory, so it
# accumulates if a container is stopped and restarted.
rm -rf /var/tmp/archivesspace

if [[ ${any_component_running:?} != true ]]; then
    echo "docker-entrypoint.sh: Error: No ArchivesSpace components are selected to run" >&2
    exit 1
fi

if [[ ${ASPACE_JAVA_HEAP:-} == '' && ${ASPACE_JAVA_MEM:-} != '' ]]; then
    read -ra java_memory_opts <<<"${ASPACE_JAVA_MEM:?}"
else
    # The default ASPACE_HEAP should be enough for the a single component for testing purposes
    ASPACE_JAVA_HEAP=${ASPACE_JAVA_HEAP:-256m}
    java_memory_opts=(
        "-Xms${ASPACE_JAVA_HEAP:?}" "-Xmx${ASPACE_JAVA_HEAP:?}"
    )
fi

if [[ ${ASPACE_JAVA_GC_LOG_OPTS:-} == '' ]]; then
    java_gc_log_opts=("-Xlog:gc*:file=${ASPACE_LOG_DIR:?}/archivesspace_gc.log:time,uptime:filecount=4,filesize=20M")
else
    read -ra java_gc_log_opts <<<"${ASPACE_JAVA_GC_LOG_OPTS:?}"
fi

if [[ ${ASPACE_JAVA_GC_OPTS:-} == '' ]]; then
    java_cg_opts=(
        -XX:+UseG1GC -XX:MaxGCPauseMillis=100
        -XX:+UseLargePages
        # write 0s to the entire heap at startup to pre-allocate space
        -XX:+AlwaysPreTouch
    )
else
    read -ra java_cg_opts <<<"${ASPACE_JAVA_GC_OPTS:?}"
fi

default_java_opts=(
    -server
    --add-opens java.base/sun.nio.ch=ALL-UNNAMED
    --add-opens java.base/java.io=ALL-UNNAMED
    # Always crash when out of memory. The program can be in an unexpected state,
    # and if it's OOM then it's probably bogged down and responding slowly.
    # The container can be restarted automatically after crashing.
    -XX:+CrashOnOutOfMemoryError
    "${java_memory_opts[@]}"
    "${java_cg_opts[@]}"
    "${java_gc_log_opts[@]}"
)
read -ra java_opts <<<"${JAVA_OPTS:-}"
classpath_entries=(
    gems/gems/jruby-*/**/*.jar
    lib/*
    launcher/lib/*.jar
)
printf -v joined_classpath '%s:' "${classpath_entries[@]:?}"

if [[ ${BASHPID:?} == 1 ]]; then
    init_program=(tini java --)
else
    init_program=(java);
fi

# Run entrypoints for plugins
readarray -d '' plugin_entrypoints < <(
    find plugins -type f -wholename '*/container-config/docker-entrypoint.sh' -executable -print0
)
for plugin_entrypoint in "${plugin_entrypoints[@]}"; do
    plugin_dir=$(dirname "$(dirname "${plugin_entrypoint:?}" )" )
    ( cd "${plugin_dir:?}" && ./container-config/docker-entrypoint.sh ) || {
        echo "ERROR: plugin entrypoint ${plugin_entrypoint@Q} failed with status $?" >&2;
        exit 1
    }
done

set -x
exec "${init_program[@]}" "${default_java_opts[@]:?}" "${java_opts[@]}" \
    -Dfile.encoding=UTF-8 \
    -cp "${joined_classpath:?}" \
    org.jruby.Main --disable-gems launcher/launcher.rb
