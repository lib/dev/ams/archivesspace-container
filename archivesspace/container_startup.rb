module ArchivesSpace

  # This is an unfortunate hack to allow components to be enabled from the point
  # of view of the application logic, but disabled from the point of view of
  # launcher.rb (which decides which components to start servers for).
  #
  # Because we run components in separate containers (separate processes), we
  # generally have all but 1 component disabled so that only the one we want
  # runs in a process. But there are places in the application that use the
  # enable_xxx options to conditionally enable features. An example is the
  # "View Published" button in the staff interface:
  # https://github.com/archivesspace/archivesspace/blob/33f8dfd43da1e2a162ef22e0e154f6af7d701d38/frontend/app/views/shared/_resource_toolbar.html.erb#L50
  #
  # Longer term we should look at supporting this properly, but a temporary
  # workaround here is to respond differently to enable_xxx settings depending
  # on whether the thread querying the config option is the launcher thread or
  # any other thread serving a request.
  module ContainerWorkarounds
    def self.mark_current_thread_as_launcher_thread
      Thread.current.thread_variable_set(:archivesspace_is_launcher_thread, true)
    end

    def self.call_is_on_the_launcher_thread?
      !!Thread.current.thread_variable_get(:archivesspace_is_launcher_thread)
    end

    def self.is_component_enabled_considering_calling_thread?(name)
      if call_is_on_the_launcher_thread?
        return AppConfig["run_#{name}".to_sym]
      else
        return AppConfig["#{name}_enabled".to_sym]
      end
    end
  end
end
